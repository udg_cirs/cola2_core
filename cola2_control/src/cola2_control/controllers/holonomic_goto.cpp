/*
 * Copyright (c) 2017 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#include <cola2_control/controllers/holonomic_goto.h>

// Constructor
HolonomicGotoController::HolonomicGotoController(HolonomicGotoControllerConfig config) : config_(config)
{
}

void HolonomicGotoController::setConfig(const HolonomicGotoControllerConfig& config)
{
  config_ = config;
}

// Compute Method
void HolonomicGotoController::compute(const control::State& current_state, const control::Waypoint& waypoint,
                                      control::State& controller_output, control::Feedback& feedback,
                                      control::PointsList& marker)
{
  // Set all axis as disabled by default
  controller_output.pose.disable_axis.x     = true;
  controller_output.pose.disable_axis.y     = true;
  controller_output.pose.disable_axis.z     = true;
  controller_output.pose.disable_axis.roll  = true;
  controller_output.pose.disable_axis.pitch = true;
  controller_output.pose.disable_axis.yaw   = true;
  controller_output.velocity.disable_axis.x     = true;
  controller_output.velocity.disable_axis.y     = true;
  controller_output.velocity.disable_axis.z     = true;
  controller_output.velocity.disable_axis.roll  = true;
  controller_output.velocity.disable_axis.pitch = true;
  controller_output.velocity.disable_axis.yaw   = true;

  // Set variables to zero
  controller_output.pose.position.north    = 0.0;
  controller_output.pose.position.east     = 0.0;
  controller_output.pose.position.depth    = 0.0;
  controller_output.pose.orientation.roll  = 0.0;
  controller_output.pose.orientation.pitch = 0.0;
  controller_output.pose.orientation.yaw   = 0.0;
  controller_output.velocity.linear.x  = 0.0;
  controller_output.velocity.linear.y  = 0.0;
  controller_output.velocity.linear.z  = 0.0;
  controller_output.velocity.angular.x = 0.0;
  controller_output.velocity.angular.y = 0.0;
  controller_output.velocity.angular.z = 0.0;

  // Take desired and current Z
  double desired_z = waypoint.position.depth;
  double current_z = current_state.pose.position.depth;
  if (waypoint.altitude_mode)
  {
    desired_z = waypoint.altitude;
    current_z = current_state.pose.altitude;
  }

  // If necessary, check if final position X, Y is reached
  feedback.success = true;
  if (!waypoint.disable_axis.x)
  {
    // X-Y tolerance must be checked
    if (std::fabs(current_state.pose.position.north - waypoint.position.north) >= waypoint.position_tolerance.x ||
        std::fabs(current_state.pose.position.east - waypoint.position.east) >= waypoint.position_tolerance.y)
    {
      feedback.success = false;
    }
  }

  // If necessary, check if final position Z is reached
  if (!waypoint.disable_axis.z && std::fabs(current_z - desired_z) >= waypoint.position_tolerance.z)
  {
    feedback.success = false;
  }

  // If necessary, check if final angle yaw is reached
  if (!waypoint.disable_axis.yaw &&
      std::fabs(cola2::utils::wrapAngle(current_state.pose.orientation.yaw - waypoint.orientation.yaw)) >=
          waypoint.orientation_tolerance.yaw)
  {
    feedback.success = false;
  }

  // Set up controller's output and feedback
  // Set desired Z
  if (!waypoint.disable_axis.z)
  {
    controller_output.pose.altitude_mode = waypoint.altitude_mode;
    controller_output.pose.altitude = waypoint.altitude;
    controller_output.pose.position.depth = waypoint.position.depth;
    controller_output.pose.disable_axis.z = false;
    feedback.desired_depth = desired_z;
  }

  // If X-Y motion is enabled
  if (!waypoint.disable_axis.x)
  {
    // Set Surge velocity
    controller_output.pose.position.north = waypoint.position.north;
    controller_output.pose.position.east = waypoint.position.east;
    controller_output.pose.disable_axis.x = false;
    controller_output.pose.disable_axis.y = false;
  }

  if (!waypoint.disable_axis.yaw)
  {
    // Set desired yaw
    controller_output.pose.orientation.yaw = waypoint.orientation.yaw;
    controller_output.pose.disable_axis.yaw = false;
    feedback.desired_yaw = waypoint.orientation.yaw;
  }

  // Fill additional feedback vars
  feedback.distance_to_end = std::sqrt(std::pow(current_state.pose.position.north - waypoint.position.north, 2) +
                                       std::pow(current_state.pose.position.east - waypoint.position.east, 2));

  // Fill marker
  control::Point initial_point;
  control::Point final_point;

  initial_point.x = current_state.pose.position.north;
  initial_point.y = current_state.pose.position.east;
  initial_point.z = current_state.pose.position.depth;
  marker.points_list.push_back(initial_point);
  final_point.x = waypoint.position.north;
  final_point.y = waypoint.position.east;
  final_point.z = waypoint.position.depth;
  marker.points_list.push_back(final_point);
}
