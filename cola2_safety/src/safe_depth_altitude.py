#!/usr/bin/env python
# Copyright (c) 2018 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.



import rospy

from cola2_msgs.msg import BodyVelocityReq
from cola2_msgs.msg import GoalDescriptor
from cola2_msgs.msg import NavSts

from cola2_lib.rosutils import param_loader
from cola2_lib.rosutils.diagnostic_helper import DiagnosticHelper

from cola2_safety.cfg import SafeDepthAltitudeConfig

from dynamic_reconfigure.server import Server
from diagnostic_msgs.msg import DiagnosticStatus

"""
 @@>Prevents the vehicle to go behind a maximum depth or a minimum altitude by sending a BodyVelocityReq asking a negative heave with maximum priority.<@@
"""
"""
Created on Fri Mar 22 2013
Modified 11/2015
@author: narcis palomeras
"""


class SafeDepthAltitude(object):
    """This node is able to check altitude and depth."""

    def __init__(self, name):
        """Init the class."""
        # Init class vars
        self.name = name

        # Get config parameters
        self.get_config()

        # Set up diagnostics
        self.diagnostic = DiagnosticHelper(self.name, "soft")

        # Publisher
        resolved_namespace = rospy.get_namespace()
        self.pub_body_velocity_req = rospy.Publisher(resolved_namespace + "controller/body_velocity_req",
                                                     BodyVelocityReq, queue_size=2)

        # Subscriber
        rospy.Subscriber(resolved_namespace + "navigator/navigation", NavSts, self.update_nav_sts, queue_size=1)

        # Create dynamic reconfigure service
        self.dynamic_reconfigure_srv = Server(SafeDepthAltitudeConfig, self.dynamic_reconfigure_callback)

        # Show message
        rospy.loginfo("%s: initialized", self.name)

    def dynamic_reconfigure_callback(self, config, level):
        """Dynamic reconfigure callback."""
        rospy.loginfo(
           "Reconfigure Request: {min_altitude}, {max_depth}".format(**config))
        self.max_depth = config.max_depth
        self.min_altitude = config.min_altitude
        return config

    def update_nav_sts(self, nav):
        """Navigation callback. It triggers the main loop."""
        self.diagnostic.add("altitude", str(nav.altitude))
        self.diagnostic.add("depth", str(nav.position.depth))

        if (nav.altitude > 0 and nav.altitude < self.min_altitude and nav.position.depth > 0.5) or \
            nav.position.depth > self.max_depth:
            # Show message
            self.diagnostic.set_level(DiagnosticStatus.WARN, 'Invalid depth/altitude! Moving vehicle up.')
            if (nav.altitude > 0 and nav.altitude < self.min_altitude and nav.position.depth > 0.5):
                rospy.logwarn("%s: invalid altitude: %s",
                              self.name, nav.altitude)
            if (nav.position.depth > self.max_depth):
                rospy.logwarn("%s: invalid depth: %s",
                              self.name, nav.position.depth)

            # Go up
            bvr = BodyVelocityReq()
            bvr.twist.linear.x = 0.0
            bvr.twist.linear.y = 0.0
            bvr.twist.linear.z = -0.5
            bvr.twist.angular.x = 0.0
            bvr.twist.angular.y = 0.0
            bvr.twist.angular.z = 0.0
            bvr.disable_axis.x = True
            bvr.disable_axis.y = True
            bvr.disable_axis.z = False
            bvr.disable_axis.roll = True
            bvr.disable_axis.pitch = True
            # If yaw is True the vehicle can rotate while it goes up
            # but if it is False then it can not be teleoperated
            bvr.disable_axis.yaw = True
            bvr.goal.priority = GoalDescriptor.PRIORITY_SAFETY_HIGH
            bvr.goal.requester = self.name
            bvr.header.stamp = rospy.Time.now()
            self.pub_body_velocity_req.publish(bvr)
        else:
            self.diagnostic.set_level(DiagnosticStatus.OK)

    def get_config(self):
        """Get config from ROS param server."""
        param_dict = {'max_depth': ('max_depth', 1.0),
                      'min_altitude': ('min_altitude', 5.0)}

        param_loader.get_ros_params(self, param_dict)


if __name__ == '__main__':
    try:
        rospy.init_node('safe_depth_altitude')
        safe_depth_altitude = SafeDepthAltitude(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
